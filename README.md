# CHVote: Common JavaFX library
[![pipeline status](https://gitlab.com/chvote2/shared-libraries/chvote-fx-common/badges/master/pipeline.svg)](https://gitlab.com/chvote2/shared-libraries/chvote-fx-common/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-fx-common&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-fx-common)

This library contains all shared JavaFx components and resources for CHVote projects based on JavaFX.

# Usage

## Add to your project

Maven:
```xml
<dependency>
    <groupId>ch.ge.ve.ui</groupId>
    <artifactId>chvote-fx-common</artifactId>
    <version>0.0.8</version>
</dependency>
```

## Splash screen

To configure a splash screen set the `javafx.preloader` system property to:
`ch.ge.ve.javafx.gui.preloader.SplashScreenPreloader` before launching the application. It is possible to add a
background image by pointing it in the `chvote-fx-common.splash-screen.image-location` system property, e.g.:

```java

import ch.ge.ve.javafx.gui.preloader.SplashScreenPreloader;
import javafx.application.Application;

public class MyJavaFXApplication extends Application {

  public static void main(String[] args) {
    // Set the background image
    System.setProperty(SplashScreenPreloader.BACKGROUND_LOCATION_PROPERTY_KEY, "/images/splash-screen.png");
    // Set the chvote-fx-common splash screen preloader
    System.setProperty("javafx.preloader", SplashScreenPreloader.class.getCanonicalName());

    // Launch the application
    launch(args);
  }

  // ...

}
```

## JavaFX control components

* *FormattedLabel*: A specialised `Label` where the printed text is formatted from an object using a formatter.
* *MessageContainer*: A component that displays an inline message to the user with 3 levels of alertness (`INFO`,
`WARNING` or `ERROR`).
* *PrivateKeyInputGroup*: A form to generate a private key.
* *PublicKeyInputGroup*: A form to select the destination folder of a public key.
* *StepperPane*: A component that provides a wizard-like linear workflow by dividing content into logical steps.

### Stepper usage

Initialize the stepper and set the steps:

```java
StepperPane stepper = new StepperPane();
stepper.getSteps().add(new Step("Step 1", new Label("Step 1 body")));
stepper.getSteps().add(new Step("Step 2", new Label("Step 2 body")));
root.getChildren().add(stepper);
```

Create a button that opens the previous step in the list:

```java
Button previousButton = new Button("Previous");
// Disable de button if there are no previous steps
previousButton.disableProperty().bind(stepper.hasPreviousProperty().not());
// On click open previous step
previousButton.setOnMouseClicked(event -> stepper.previous());
```

Create a button that opens the next step in the list:

```java
Button nextButton = new Button("Next");
// Disable de button if there are no next steps
nextButton.disableProperty().bind(stepper.hasNextProperty().not());
// On click open next step
nextButton.setOnMouseClicked(event -> stepper.next());
```

# Building

## Pre-requisites

* JDK 11
* Maven

## Build steps

```bash
mvn clean install
```

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# Licence
This application is Open Source software released under the [Affero General Public License 3.0](https://gitlab.com/chvote2/shared-libraries/chvote-fx-common/blob/master/LICENSE) 
license.
