/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.json;

import ch.ge.ve.javafx.business.json.exception.JsonGenerationException;
import ch.ge.ve.javafx.business.json.exception.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A utility class to deserialize a JSON content from given {@link Path}.
 */
@Service
public class JsonPathMapper {
  private final ObjectMapper objectMapper;

  /**
   * Create a new {@link JsonPathMapper} instance.
   *
   * @param objectMapper the underlying object mapper.
   */
  @Autowired
  public JsonPathMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  /**
   * Deserialize the given {@link Path} into the given Java type.
   *
   * @param source the file to deserialize.
   * @param type   the resulting object type.
   * @param <T>    the resulting object type.
   *
   * @return the deserialized object.
   *
   * @throws JsonParseException if there was a problem parsing the given source.
   */
  public <T> T map(Path source, Class<T> type) {
    try (InputStream in = Files.newInputStream(source)) {
      return objectMapper.readValue(in, type);
    } catch (IOException e) {
      throw new JsonParseException(
          String.format("File [%s] could not be mapped to type: [%s]", source, type), e);
    }
  }

  /**
   * Deserialize the given {@link Path} into the given Java type reference.
   *
   * @param source the file to deserialize.
   * @param type   the type reference.
   * @param <T>    the resulting object type.
   *
   * @return the deserialized object.
   *
   * @throws JsonParseException if there was a problem parsing the given source.
   */
  public <T> T map(Path source, TypeReference<T> type) {
    try (InputStream in = Files.newInputStream(source)) {
      return objectMapper.readValue(in, type);
    } catch (IOException e) {
      throw new JsonParseException(
          String.format("File [%s] could not be mapped to type: [%s]", source, type), e);
    }
  }

  /**
   * Serialize the given object into the specified destination {@link Path}.
   *
   * @param destination the destination file.
   * @param value       the value to write
   *
   * @throws JsonGenerationException if there was a problem writing the given object to its destination.
   */
  public void write(Path destination, Object value) {
    try (OutputStream out = Files.newOutputStream(destination)) {
      objectMapper.writeValue(out, value);
    } catch (IOException e) {
      throw new JsonGenerationException(
          String.format("Cannot write object [%s] to file: [%s]", value, destination), e);
    }
  }

}
