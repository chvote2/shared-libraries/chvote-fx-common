/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.xml;

import ch.ge.ve.javafx.business.xml.exception.InvalidVoterChoicesException;
import ch.ge.ve.model.convert.api.MissingElectionIdentifierException;
import ch.ge.ve.model.convert.api.VoterChoiceConverter;
import ch.ge.ve.model.convert.model.VoterChoice;
import ch.ge.ve.protocol.model.ElectionSet;
import ch.ge.ve.protocol.model.PrintingAuthority;
import com.google.common.io.Closer;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A {@link VoterChoice} factory.
 */
@Service
public class VoterChoicesFactory {

  private final VoterChoiceConverter voterChoiceConverter;

  /**
   * Create a new {@link VoterChoicesFactory} instance.
   *
   * @param voterChoiceConverter the voter choice converter strategy.
   */
  @Autowired
  public VoterChoicesFactory(VoterChoiceConverter voterChoiceConverter) {
    this.voterChoiceConverter = voterChoiceConverter;
  }

  /**
   * Convert the contents of the list of operation references (eCH-0159 and eCH-0157) paths into a list of voter choices
   * in the order defined by the given election set.
   *
   * @param electionSet             the election set.
   * @param operationReferenceFiles the files containing the operation references xml data.
   *
   * @return a list of voter choices corresponding to the combination of the election set and the operation reference
   * sources.
   *
   * @throws InvalidVoterChoicesException if the voter choice list cannot be generated.
   */
  public List<VoterChoice> createVoterChoices(ElectionSet<? extends PrintingAuthority> electionSet,
                                              List<Path> operationReferenceFiles) {
    List<InputStream> deliveryStreams = new ArrayList<>();

    try (Closer closer = Closer.create()) {
      for (Path operationReferenceFile : operationReferenceFiles) {
        InputStream in = closer.register(Files.newInputStream(operationReferenceFile));
        deliveryStreams.add(in);
      }

      return voterChoiceConverter.convertToVoterChoiceList(
          electionSet, deliveryStreams.toArray(new InputStream[deliveryStreams.size()])
      );
    } catch (IOException | MissingElectionIdentifierException e) {
      throw new InvalidVoterChoicesException("Cannot parse operation reference files", e);
    }
  }
}
