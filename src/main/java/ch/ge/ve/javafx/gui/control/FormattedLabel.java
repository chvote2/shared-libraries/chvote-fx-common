/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.control;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;

/**
 * Extension of the {@link Label} control, where the printed text is formatted from an object using a formatter.
 *
 * @param <T> type of bound object.
 */
public class FormattedLabel<T> extends Label {

  private final ObjectProperty<StringFormatter<T>> formatter;
  private final ObjectProperty<T>                  value;

  /**
   * Create an empty label.
   */
  public FormattedLabel() {
    super();

    this.formatter = new SimpleObjectProperty<>(this, "formatter");
    this.value = new SimpleObjectProperty<>(this, "value");

    valueProperty().addListener((observable, oldValue, newValue) -> {
      final StringFormatter<T> currentFormatter = formatter.get();

      String strValue = (currentFormatter == null) ? String.valueOf(newValue) : currentFormatter.asString(newValue);
      FormattedLabel.super.setText(strValue);
    });
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the current string formatter for this label.
   *
   * @return the current string formatter for this label.
   */
  public StringFormatter<T> getFormatter() {
    return formatter.get();
  }

  /**
   * Set the current string formatter for this label. Setting the formatter after the value will have no effect on this
   * label's text.
   *
   * @param formatter he current string formatter for this label.
   */
  public void setFormatter(StringFormatter<T> formatter) {
    this.formatter.set(formatter);
  }

  /**
   * The string formatter property.
   *
   * @return the string formatter property.
   */
  public ObjectProperty<StringFormatter<T>> formatterProperty() {
    return formatter;
  }

  /**
   * Get the current value of this formatted label.
   *
   * @return the current value fo this formatted label.
   */
  public T getValue() {
    return value.get();
  }

  /**
   * Set the current value of this formatted label. Whenever this property changes this label text is formatted using to
   * the current {@link StringFormatter} strategy, if the {@link StringFormatter} has not yet been set this value will
   * be used as the label's text without any further modification.
   *
   * @param value the value fo this formatted label.
   *
   * @see StringFormatter#asString(Object)
   */
  public void setValue(T value) {
    this.value.set(value);
  }

  /**
   * The value property for this formatted label.
   *
   * @return the value property for this formatted label.
   */
  public ObjectProperty<T> valueProperty() {
    return value;
  }
}
