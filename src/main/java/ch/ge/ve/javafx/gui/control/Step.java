/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.control;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;

/**
 * A specialized {@link Tab} that allows setting the tooltip through a {@link #tooltipTextProperty()}.
 */
public class Step extends Tab {

  private static final String DEFAULT_STYLE_CLASS = "chvote-step";

  private final StringProperty tooltipText;

  /**
   * Create an empty {@link Step}.
   */
  public Step() {
    this(null);
  }

  /**
   * Create a {@link Step} with a text title.
   *
   * @param text The title of the step.
   */
  public Step(String text) {
    this(text, null);
  }

  /**
   * Create a {@link Step} with a text title and the specified content node.
   *
   * @param text    The title of the step.
   * @param content The content of the step.
   */
  public Step(String text, Node content) {
    super(text, content);

    this.tooltipText = new SimpleStringProperty(this, "tooltipText");

    initialize();
  }

  private void initialize() {
    getStyleClass().add(DEFAULT_STYLE_CLASS);
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Set the tooltip text for this step.
   *
   * @param value the tooltip text.
   */
  public final void setTooltipText(String value) {
    tooltipTextProperty().setValue(value);

    Tooltip tooltip = new Tooltip();
    tooltip.setText(value);
    tooltipProperty().setValue(tooltip);
  }

  /**
   * Get the tooltip text for this step.
   *
   * @return the tooltip text.
   */
  public final String getTooltipText() {
    return tooltipText.getValue();
  }

  /**
   * Get the tooltip text property.
   *
   * @return the tooltip text property.
   */
  public final StringProperty tooltipTextProperty() {
    return tooltipText;
  }

}