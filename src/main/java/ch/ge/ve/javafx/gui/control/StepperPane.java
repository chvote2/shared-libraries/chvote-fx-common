/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.control;

import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

/**
 * A stepper pane provides a wizard-like linear workflow by dividing content into logical steps, steps are expected to
 * be added to te list returned by the {@link #getSteps()} method.
 */
@DefaultProperty("steps")
public class StepperPane extends VBox {

  private static final String DEFAULT_STYLE_CLASS = "chvote-stepper-pane";

  private CustomJFXTabPane tabPane;

  private final ObservableList<Step> steps;
  private final IntegerProperty      currentPosition;
  private final BooleanProperty      hasNext;
  private final BooleanProperty      hasPrevious;

  /**
   * Create a new stepper pane instance.
   */
  public StepperPane() {
    this.steps = FXCollections.observableArrayList();
    this.currentPosition = new SimpleIntegerProperty(this, "currentPosition");
    this.hasNext = new SimpleBooleanProperty(this, "hasNext");
    this.hasPrevious = new SimpleBooleanProperty(this, "hasPrevious");

    tabPane = new CustomJFXTabPane();
    getChildren().add(tabPane);
    initialize();
  }

  private void initialize() {
    getStyleClass().setAll(DEFAULT_STYLE_CLASS);

    SimpleIntegerProperty stepsSize = new SimpleIntegerProperty();
    steps.addListener((ListChangeListener.Change<? extends Tab> c) -> {
      tabPane.getTabs().clear();
      tabPane.getTabs().addAll(steps);
      selectStep(0);
      stepsSize.setValue(steps.size());
    });

    hasNext.bind(currentPosition.lessThan(stepsSize.subtract(1)));
    hasPrevious.bind(currentPosition.greaterThan(0));
  }

  /**
   * Select the next step, same as:
   * <p>
   * {@code stepper.setCurrentPosition(stepper.getCurrentPosition() + 1)}
   * </p>
   */
  public final void next() {
    selectStep(currentPositionProperty().get() + 1);
  }

  /**
   * Select the previous step, same as:
   * <p>
   * {@code stepper.setCurrentPosition(stepper.getCurrentPosition() - 1)}
   * </p>
   */
  public final void previous() {
    selectStep(currentPositionProperty().get() - 1);
  }

  private void selectStep(int index) {
    int lastTabIndex = steps.size() - 1;
    steps.forEach(step -> step.setDisable(true));

    if (steps.isEmpty()) {
      currentPositionProperty().setValue(0);
      hasNextProperty().setValue(false);
      hasPreviousProperty().setValue(false);
    } else if (index >= 0 && index <= lastTabIndex) {
      currentPositionProperty().setValue(index);

      Step selectedStep = steps.get(index);
      selectedStep.setDisable(false);
      tabPane.getSelectionModel().select(selectedStep);
    } else {
      throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + steps.size());
    }
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the steps to display in this stepper pane. Changing this {@link ObservableList} will immediately result in the
   * stepper pane updating to display the new contents of this {@link ObservableList} and automatically selecting the
   * first step on this list.
   *
   * @return the steps of this stepper pane.
   */
  public final ObservableList<Step> getSteps() {
    return steps;
  }

  /**
   * Get the position of the currently active step.
   *
   * @return the position of the currently active step.
   */
  public final Integer getCurrentPosition() {
    return currentPosition == null ? null : currentPosition.get();
  }

  /**
   * Select an arbitrary step by its position.
   *
   * @param value the position of the step to select.
   */
  public final void setCurrentPosition(Integer value) {
    selectStep(value);
  }

  /**
   * Get the current position property.
   *
   * @return the current position property.
   */
  public final IntegerProperty currentPositionProperty() {
    return currentPosition;
  }

  /**
   * Whether there is a next step or not.
   *
   * @return true if there is a next step, false if it's the last step of the stepper.
   */
  public final boolean getHasNext() {
    return hasNext.get();
  }

  /**
   * The has next property.
   *
   * @return the has next property
   */
  public final BooleanProperty hasNextProperty() {
    return hasNext;
  }

  /**
   * Whether there is a previous step or not.
   *
   * @return true if there is a previous step, false if it's the first step of the stepper.
   */
  public final boolean getHasPrevious() {
    return hasPrevious.get();
  }

  /**
   * Get the has previous property.
   *
   * @return the has previous property
   */
  public final BooleanProperty hasPreviousProperty() {
    return hasPrevious;
  }
}