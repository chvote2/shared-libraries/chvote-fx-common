/*
 * #%L
 * chvote-fx-common
 * %%
 * Copyright (C) 2016 - 2018 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.javafx.gui.preloader;

import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * A javafx preloader with a splash screen.
 *
 * <p>
 * To customize the splash screen's background image set the {@code chvote-fx-common.splash-screen.image-location}
 * system property (defined by {@link #BACKGROUND_LOCATION_PROPERTY_KEY}) to the location of the custom image, e.g.:
 * </p>
 *
 * {@code System.setProperty("chvote-fx-common.splash-screen.image-location", "/images/splash-screen.png");}
 *
 * <p>
 * The size of the splash screen is fixed to 390x390 pixels, the images will be resized to fit this constraint.
 * </p>
 *
 * <p>
 * To use this preloader, set the {@code javafx.preloader} system property, to do it at runtime simply execute:
 * <code>System.setProperty("javafx.preloader", SplashScreenPreloader.class.getCanonicalName());</code> before
 * launching the javafx application.
 * </p>
 */
public class SplashScreenPreloader extends Preloader {
  /**
   * The system property key for the splash screen image location.
   */
  public static final String BACKGROUND_LOCATION_PROPERTY_KEY = "chvote-fx-common.splash-screen.image-location";

  private Stage stage;

  private Scene createPreloaderScene() {
    StackPane p = new StackPane();
    p.setStyle(
        String.format("-fx-background-image: url(\"%s\")",
                      System.getProperty(BACKGROUND_LOCATION_PROPERTY_KEY))
    );
    p.getStyleClass().add("chvote-splash-screen");
    p.getChildren().add(new ProgressIndicator());
    return new Scene(p, 390, 390);
  }

  @Override
  public void start(Stage stage) {
    Scene scene = createPreloaderScene();
    scene.getStylesheets().add("css/chvote-common.css");
    stage.setScene(scene);

    stage.initStyle(StageStyle.UNDECORATED);
    stage.show();

    this.stage = stage;
  }

  @Override
  public void handleStateChangeNotification(StateChangeNotification stateChangeNotification) {
    if (stateChangeNotification.getType() == StateChangeNotification.Type.BEFORE_START) {
      stage.hide();
    }
  }
}