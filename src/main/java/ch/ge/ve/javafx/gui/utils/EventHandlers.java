/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.utils;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import java.util.Optional;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * An {@link EventHandler} utility class.
 */
public final class EventHandlers {

  /**
   * Create an {@link EventHandler} that freezes the current {@link WindowEvent} and shows a dialog to the user asking
   * him for confirmation on exiting the application.
   *
   * @param stage the {@link Stage} of the application.
   *
   * @return the {@link EventHandler}.
   */
  public static EventHandler<WindowEvent> createExitDialogEventHandler(Stage stage) {
    return event -> {
      Alert exitDialog = new Alert(
          Alert.AlertType.CONFIRMATION,
          LanguageUtils.getCurrentResourceBundle().getString("exit-dialog.are-you-sure")
      );
      exitDialog.setTitle(LanguageUtils.getCurrentResourceBundle().getString("exit-dialog.title"));
      exitDialog.setHeaderText(null);
      exitDialog.initModality(Modality.APPLICATION_MODAL);
      exitDialog.initOwner(stage);

      Button exitButton = (Button) exitDialog.getDialogPane().lookupButton(ButtonType.OK);
      exitButton.setText(LanguageUtils.getCurrentResourceBundle().getString("exit-dialog.exit"));

      Button cancelButton = (Button) exitDialog.getDialogPane().lookupButton(ButtonType.CANCEL);
      cancelButton.setText(LanguageUtils.getCurrentResourceBundle().getString("exit-dialog.cancel"));

      Optional<ButtonType> closeResponse = exitDialog.showAndWait();

      if (!ButtonType.OK.equals(closeResponse.get())) {
        event.consume();
      }
    };
  }

  /**
   * Hide utility class constructor.
   */
  private EventHandlers() {
    throw new AssertionError("Not instantiable");
  }
}
