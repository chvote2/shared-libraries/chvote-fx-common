/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.utils;

import com.google.common.base.Strings;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.springframework.stereotype.Service;

/**
 * A service that provides file selection opening a JavaFX dialog. The service always opens a dialog on the last visited
 * directory of the previous call.
 */
@Service
public class FileBrowserService {
  private File lastLookedUpDirectory;

  private FileChooser getFileChooser(String extension) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setInitialDirectory(lastLookedUpDirectory);
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(extension, "*." + extension));
    return fileChooser;
  }

  private Path toPath(JFXTextField textField) {
    return Strings.isNullOrEmpty(textField.getText()) ? null : Paths.get(textField.getText());
  }

  /**
   * Open a dialog that allows the selection multiple files. The absolute path of each file is then written in the given
   * {@link JFXTextArea}. The files are filtered using the given extension.
   *
   * @param textArea  the text field where the selected files path will be shown.
   * @param extension the extension filter for the dialog.
   *
   * @return the list fo selected files.
   */
  public List<Path> selectMultipleFiles(JFXTextArea textArea, String extension) {
    List<Path> result = new ArrayList<>();
    textArea.resetValidation();

    List<File> files = getFileChooser(extension).showOpenMultipleDialog(textArea.getScene().getWindow());

    if (files != null) {
      result = files.stream().map(File::toPath).collect(Collectors.toList());

      textArea.setText(
          result.stream()
                .map(Path::toAbsolutePath)
                .map(Path::toString)
                .collect(Collectors.joining("\n"))
      );
      textArea.setPrefRowCount((!result.isEmpty()) ? result.size() : 1);
      lastLookedUpDirectory = files.get(0).getParentFile();
    } else {
      textArea.setText(null);
      textArea.setPrefRowCount(1);
    }

    textArea.requestLayout();

    textArea.validate();

    return result;
  }

  /**
   * Open a dialog that allows the selection of a single file. The absolute path of the selected file is then written in
   * the given {@link JFXTextField}. The files are filtered using the given extension.
   *
   * @param textField the text field where the selected files path will be shown.
   * @param extension the extension filter for the dialog.
   *
   * @return the selected file.
   */
  public Path selectFile(JFXTextField textField, String extension) {
    textField.resetValidation();

    Optional.ofNullable(getFileChooser(extension).showOpenDialog(textField.getScene().getWindow()))
            .map(File::toPath)
            .ifPresent(path -> {
              textField.setText(path.toAbsolutePath().toString());
              lastLookedUpDirectory = path.getParent().toFile();
            });

    textField.validate();

    return toPath(textField);
  }

  /**
   * Open a dialog that allows the selection of a single directory. The absolute path of the selected directory is then
   * written in the given {@link JFXTextField}.
   *
   * @param textField the text field where the selected files path will be shown.
   *
   * @return the selected directory.
   */
  public Path selectDirectory(JFXTextField textField) {
    textField.resetValidation();

    DirectoryChooser directoryChooser = new DirectoryChooser();
    directoryChooser.setInitialDirectory(lastLookedUpDirectory);
    Optional.ofNullable(directoryChooser.showDialog(textField.getScene().getWindow()))
            .ifPresent(directory -> {
              textField.setText(directory.getAbsolutePath());
              lastLookedUpDirectory = directory;
            });

    textField.validate();

    return toPath(textField);
  }
}
