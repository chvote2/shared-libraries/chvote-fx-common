/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.utils;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;
import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import java.text.MessageFormat;
import java.util.stream.Stream;
import javafx.concurrent.Task;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility class that adds visual feedback on successful and unsuccessful {@link Task} termination.
 */
public class TaskUtils {
  private static final Logger logger = LoggerFactory.getLogger(TaskUtils.class);

  private static final String VALID_GLYPH   = "valid-glyph";
  private static final String INVALID_GLYPH = "invalid-glyph";

  /**
   * Adds a {@link MaterialIcon#CLEAR} icon to the given progress bar container.
   *
   * @param throwable            the exception thrown during the task's execution.
   * @param progressBarContainer the progress bar container.
   */
  public static void onFailed(Throwable throwable, Pane progressBarContainer) {
    logger.error("Background task failed.", throwable);
    MaterialIconView icon = new MaterialIconView(MaterialIcon.CLEAR, "14px");
    icon.setStyleClass(INVALID_GLYPH);
    progressBarContainer.getChildren().add(icon);
  }

  /**
   * Adds a {@link MaterialIcon#CLEAR} icon to the given progress bar container.
   *
   * @param task                 the task that has failed.
   * @param progressBarContainer the progress bar container.
   * @param <T>                  the returning type of the task.
   */
  public static <T> void onFailed(Task<T> task, Pane progressBarContainer) {
    onFailed(task.getException(), progressBarContainer);
  }

  /**
   * Adds a {@link MaterialIcon#CHECK} icon to the given progress bar container.
   *
   * @param progressBarContainer the progress bar container.
   */
  public static void onSucceeded(Pane progressBarContainer) {
    MaterialIconView icon = new MaterialIconView(MaterialIcon.DONE, "14px");
    icon.setStyleClass(VALID_GLYPH);
    progressBarContainer.getChildren().add(icon);
  }

  /**
   * Remove all icons of the given progress bar containers.
   *
   * @param progressBarContainers The progress bar containers.
   */
  public static void clearAllIcons(Pane... progressBarContainers) {
    Stream.of(progressBarContainers).forEach(
        container -> container.getChildren().removeIf(node -> MaterialIconView.class.equals(node.getClass()))
    );
  }

  /**
   * The same as calling:
   * <p>
   * <code>
   * formateErrorMessage(translationKey, task.getException());
   * </code>
   * <p>
   * See {@link #formatErrorMessage(String, Throwable)}.
   *
   * @param translationKey the translation key.
   * @param task           the task that holds the exception with the error code.
   * @param <T>            the type of result returned by the task.
   *
   * @return the formatted error message.
   */
  public static <T> String formatErrorMessage(String translationKey, Task<T> task) {
    return formatErrorMessage(translationKey, task.getException());
  }

  /**
   * Formats the error message stored in the given translation key. The message must be {@link MessageFormat} compliant
   * and have a single argument, e.g.:
   * <p>
   * <code>
   * "This is the error code: {0}"
   * </code>
   * <p>
   * The rendered argument will be {@link ErrorCodeBasedException#getErrorCode()} if the given throwable is an instance
   * of {@link ErrorCodeBasedException}, otherwise is will default to: <code>UNKNOWN</code>.
   *
   * @param translationKey the translation key.
   * @param ex             the exception with the error code.
   *
   * @return the formatted error message.
   */
  public static String formatErrorMessage(String translationKey, Throwable ex) {
    return MessageFormat.format(
        LanguageUtils.getCurrentResourceBundle().getString(translationKey),
        (ex instanceof ErrorCodeBasedException) ? ((ErrorCodeBasedException) ex).getErrorCode() : "UNKNOWN"
    );
  }

  /**
   * Hide utility class constructor.
   */
  private TaskUtils() {
    throw new AssertionError("Not instantiable");
  }
}