/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.validator;

import com.google.common.base.Strings;
import com.jfoenix.validation.base.ValidatorBase;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;

/**
 * When this validator is added to a {@link TextInputControl} field it validates that its text matches the passphrase
 * set through {@link #passphraseProperty()}. Null and empty strings are considered equal.
 */
public class PassphraseConfirmationValidator extends ValidatorBase {

  private final StringProperty passphrase = new SimpleStringProperty();

  @Override
  protected void eval() {
    if (srcControl.get() instanceof TextInputControl) {
      String text = ((TextInputControl) srcControl.get()).getText();

      if (!Strings.isNullOrEmpty(getPassphrase())) {
        hasErrors.set(!getPassphrase().equals(text));
      } else {
        hasErrors.set(Strings.isNullOrEmpty(text));
      }
    }
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Set the expected passphrase.
   *
   * @param value the passphrase.
   */
  public final void setPassphrase(String value) {
    passphrase.set(value);
  }

  /**
   * Get the expected passphrase.
   *
   * @return the expected passphrase.
   */
  public final String getPassphrase() {
    return passphrase.get();
  }

  /**
   * The expected passphrase property.
   *
   * @return the expected passphrase property
   */
  public final StringProperty passphraseProperty() {
    return passphrase;
  }
}
