/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer
import ch.ge.ve.javafx.business.json.JsonPathMapper
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import groovy.transform.CompileStatic
import org.bouncycastle.jce.provider.BouncyCastleProvider

import java.nio.file.Path
import java.nio.file.Paths
import java.security.Security

@CompileStatic
class TestUtils {
  static Path RESOURCES_ROOT = Paths.get("src", "test", "resources")
  static Path ECH0159_FOLDER = RESOURCES_ROOT.resolve("ech0159")
  static Path ELECTION_SET_FOLDER = RESOURCES_ROOT.resolve("election-set")
  static Path JSON_FOLDER = RESOURCES_ROOT.resolve("json")

  static {
    Security.addProvider(new BouncyCastleProvider())
  }

  static ObjectMapper createObjectMapper() {
    SimpleModule module = new SimpleModule()

    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())

    return new ObjectMapper().registerModule(module)
  }

  static JsonPathMapper createJsonPathMapper() {
    return new JsonPathMapper(createObjectMapper())
  }
}
