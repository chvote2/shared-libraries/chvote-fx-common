/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.json

import ch.ge.ve.javafx.TestUtils
import ch.ge.ve.javafx.business.json.exception.JsonParseException
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey
import com.fasterxml.jackson.core.type.TypeReference
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class JsonPathMapperTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder

  JsonPathMapper jsonPathMapper

  def setup() {
    jsonPathMapper = new JsonPathMapper(TestUtils.createObjectMapper())
  }

  def "should map the given Json file into an object"() {
    given:
    def source = TestUtils.ELECTION_SET_FOLDER.resolve("201X04VE-election-set.json")

    when:
    def result = jsonPathMapper.map(source, ElectionSetWithPublicKey.class)

    then:
    result != null
    result.elections.size() > 0
  }

  def "should fail to map an invalid file"() {
    given:
    def source = TestUtils.ELECTION_SET_FOLDER.resolve("201X04VE-election-set-invalid.json")

    when:
    jsonPathMapper.map(source, ElectionSetWithPublicKey.class)

    then:
    def ex = thrown(JsonParseException)
    ex.getErrorCode() != null
  }

  def "should map the given Json file into a HashMap using a type reference"() {
    given:
    def source = TestUtils.JSON_FOLDER.resolve("string-map.json")

    when:
    def result = jsonPathMapper.map(source, new TypeReference<HashMap<String, String>>() {})

    then:
    result != null
    result.key1 == "value1"
  }

  def "should write the given object into a json file"() {
    given:
    def value = ["value0", "value1"].asList()
    def output = temporaryFolder.newFile().toPath()

    when:
    jsonPathMapper.write(output, value)

    then:
    output.text == '["value0","value1"]'
  }


}
