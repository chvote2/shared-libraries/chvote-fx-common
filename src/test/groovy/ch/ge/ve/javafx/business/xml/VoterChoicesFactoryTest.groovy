/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.xml

import ch.ge.ve.javafx.TestUtils
import ch.ge.ve.model.convert.impl.DefaultVoterChoiceConverter
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey
import java.nio.file.Path
import spock.lang.Specification

class VoterChoicesFactoryTest extends Specification {
  VoterChoicesFactory voterChoicesFactory

  def setup() {
    voterChoicesFactory = new VoterChoicesFactory(new DefaultVoterChoiceConverter())
  }

  def "should create a voter choice list given and election set and its eCH-0159 reference file"() {
    given:
    def ech0159File = TestUtils.ECH0159_FOLDER.resolve("201X04VE-ech0159.xml")
    def electionSet = createElectionSet(TestUtils.ELECTION_SET_FOLDER.resolve("201X04VE-election-set.json"))

    when:
    def choices = voterChoicesFactory.createVoterChoices(electionSet, Arrays.asList(ech0159File))

    then:
    choices != null
    choices.size() == electionSet.getCandidates().size()
  }

  def createElectionSet(Path path) {
    return TestUtils.createJsonPathMapper().map(path, ElectionSetWithPublicKey.class)
  }
}
