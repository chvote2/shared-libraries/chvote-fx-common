/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.demo;

import ch.ge.ve.javafx.gui.control.Step;
import ch.ge.ve.javafx.gui.control.StepperPane;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Control;

public class StepperPaneDemoLayoutController extends Control {
  @FXML
  private StepperPane stepperPane;

  @FXML
  private Button previousButton;

  @FXML
  private Button nextButton;

  @FXML
  public void initialize() {
    ObservableList<Step> steps = stepperPane.getSteps();
    steps.clear();
    List<Step> newSteps =
        IntStream.rangeClosed(1, 10).mapToObj(i -> new Step(String.format("A very long name %d", i)))
                 .collect(Collectors.toList());
    steps.addAll(newSteps);

    previousButton.disableProperty().bind(stepperPane.hasPreviousProperty().not());
    nextButton.disableProperty().bind(stepperPane.hasNextProperty().not());
  }

  @FXML
  public void previous() {
    stepperPane.previous();
  }

  @FXML
  public void next() {
    stepperPane.next();
  }
}
